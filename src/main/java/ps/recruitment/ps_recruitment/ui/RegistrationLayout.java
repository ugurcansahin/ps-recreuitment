package ps.recruitment.ps_recruitment.ui;

import java.util.List;

import com.vaadin.server.ThemeResource;
import com.vaadin.ui.Button;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.Image;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.themes.ValoTheme;

import ps.recruitment.ps_recruitment.model.Major;
import ps.recruitment.ps_recruitment.model.Student;
import ps.recruitment.ps_recruitment.model.University;

public abstract class RegistrationLayout extends VerticalLayout {

	private List<University> universityList;

	public RegistrationLayout(List<University> universityList) {
		this.universityList = universityList;
		build();
	}

	private void build() {

		TextField tfName = new TextField("Student First Name");

		TextField tfLastName = new TextField("Student Last Name");

		TextField tfID = new TextField("Citizen ID");

		ComboBox<Major> cbMajor = new ComboBox<>("Major");
		cbMajor.setEmptySelectionAllowed(false);
		cbMajor.setItemCaptionGenerator(Major::getName);

		ComboBox<University> cbUniversity = new ComboBox<>("University");
		cbUniversity.setEmptySelectionAllowed(false);
		cbUniversity.setItemCaptionGenerator(University::getName);
		cbUniversity.addValueChangeListener(e -> {
			cbMajor.clear();
			cbMajor.setItems(cbUniversity.getValue().getMajorList());
		});
		cbUniversity.setItems(universityList);

		Button btnGenerate = new Button("Register");
		btnGenerate.addStyleName(ValoTheme.BUTTON_PRIMARY);
		btnGenerate.addClickListener(e -> {
			if (tfName.getValue().trim().isEmpty() || tfLastName.getValue().trim().isEmpty()
					|| tfID.getValue().trim().isEmpty() || cbMajor.getValue() == null
					|| cbUniversity.getValue() == null) {
				Notification.show("Please fill all the required fields!", Notification.Type.WARNING_MESSAGE);
			} else {
				Student student = register(tfName.getValue(), tfLastName.getValue(), tfID.getValue(),
						cbUniversity.getValue(), cbMajor.getValue());
				showID(student);
			}
		});

		Image image = new Image(null, new ThemeResource("logoPS.png"));
		image.setHeight("70px");

		FormLayout formLayout = new FormLayout();
		formLayout.addComponents(tfName, tfLastName, tfID, cbUniversity, cbMajor, btnGenerate);
		addComponents(image, formLayout);

	}

	private void showID(Student student) {

		// TODO please set values of labels from student object's fields

		Label lblName = new Label();
		lblName.setCaption("Name:");
		// set name here
		lblName.setValue("");

		Label lblLastName = new Label();
		lblLastName.setCaption("Last Name:");
		// set last name here
		lblLastName.setValue("");

		Label lblStudentId = new Label();
		lblStudentId.setCaption("Student ID:");
		// set student id here
		lblStudentId.setValue("");

		Label lblUniversity = new Label();
		lblUniversity.setCaption("University:");
		// set university name here
		lblUniversity.setValue("");

		Label lblMajor = new Label();
		lblMajor.setCaption("Major:");
		// set major name here
		lblMajor.setValue("");

		FormLayout idLayout = new FormLayout();
		idLayout.addStyleName("student-card");
		idLayout.addComponents(lblName, lblLastName, lblStudentId, lblUniversity, lblMajor);

		CssLayout contentLayout = new CssLayout();
		contentLayout.setWidth("500px");
		contentLayout.setHeight("500px");
		contentLayout.addStyleName("content-layout");
		contentLayout.addComponent(idLayout);

		Window window = new Window("Student Card");
		window.setContent(contentLayout);
		window.setModal(true);
		window.setResizable(false);

		UI.getCurrent().addWindow(window);

	}

	protected abstract Student register(String name, String lastName, String id, University university, Major major);

}
