package ps.recruitment.ps_recruitment.ui;

import com.vaadin.shared.ui.ContentMode;
import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;

public class RequirementsLayout extends VerticalLayout {

	public RequirementsLayout() {
		build();
	}

	private void build() {

		setWidth("700px");

		Label lblAssignment = new Label("<b>Assignment: Student registration<b>");
		lblAssignment.setContentMode(ContentMode.HTML);
		lblAssignment.addStyleName(DESIGN_ATTR_PLAIN_TEXT);
		lblAssignment.addStyleName("lbl-assignment");

		Label lblInfo = new Label(
				"What we expect is, after clicking 'Register' button a Student object will be created with a unique Student Number.</br></br>"
						+ "To be able to select a university we expect you to fill the university list at the <b>MyUI.java</b> class with at least two universities. These universities also should have at least two majors.</br></br> "
						+ "There is a method named <b>registration()</b> at <b>MyUI.java</b> class which you should implement and "
						+ "return the student object with the provided informations on the Registration Form.</br></br>"
						+ "After implementing this method you should also complete the implementation of showID(Student student) method which shows "
						+ "the Student's Informations as a Student Card at UI. For this method we only expect you to fill the label values, "
						+ "Please check the <b>RegistrationLayout.java</b> class for more information.</br></br>"
						+ "You can find the necessary classes and fields below.");
		lblInfo.setWidth(100, Unit.PERCENTAGE);
		lblInfo.setContentMode(ContentMode.HTML);

		Label lblEntities = new Label(
				"<b><u>Student (must extend to Citizen)</u></b> <li>Student ID (will be generated, must be unique)</li><li>University</li><li>Major</li></br> "
						+ "<b><u>Citizen</u></b> <li>Name</li><li>Last Name</li><li>Citizen ID</li></br>"
						+ " <b><u>University</u></b> <li>Name</li><li>List of available Majors</li></br> "
						+ "<b><u>Major</u></b> <li>Name</li>");
		lblEntities.setContentMode(ContentMode.HTML);

		setSpacing(true);
		addComponents(lblAssignment, lblInfo, lblEntities);
	}
}
