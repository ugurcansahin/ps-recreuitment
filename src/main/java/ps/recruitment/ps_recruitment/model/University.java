package ps.recruitment.ps_recruitment.model;

import java.util.List;

public class University {

	private String name;
	private List<Major> majorList;

	public List<Major> getMajorList() {
		return majorList;
	}

	public String getName() {
		return name;
	}
}
